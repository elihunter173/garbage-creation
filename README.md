# Garbage-Creation

> Just Eli playing around in bash!

## Authors

- Eli W. Hunter

## Getting Started

Just clone the repo and run the scripts!

```shell
git clone https://gitlab.com/elihunter173/garbage_creation.git
cd Garbage-Creation
./garbage_creation.sh
./garbage_cleanup.sh
```
